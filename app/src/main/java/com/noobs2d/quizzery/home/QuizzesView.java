package com.noobs2d.quizzery.home;

import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public interface QuizzesView {

    public void navigateToAddQuestionsScreen(Quiz quiz);

    public void showCreateQuizDialog();

    public void setProgressBarVisible(boolean visible);

    public void setNoQuizzesPromptVisible(boolean visible);

    public void setQuizzesListVisible(boolean visible);

    public void setCreateQuizFabVisible(boolean visible);
}
