package com.noobs2d.quizzery.framework;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * @author Julious Igmen
 */
public interface ScreenNavigator {

    public void addScreen(Fragment screen);

    public void navigateToScreen(Fragment screen);

    public void removeAllOtherScreens();

    public void setToolbarTitle(String title);

    public void showDialog(DialogFragment dialog);
}
