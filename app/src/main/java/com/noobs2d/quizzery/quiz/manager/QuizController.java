package com.noobs2d.quizzery.quiz.manager;

import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.util.DatabaseController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Julious Igmen
 */
public final class QuizController {

    // README one possible fatal problem with the structure of the static-class Table specs below is that if the
    // precedence of the columns on getAllColumnNames() differ from getAllColumns() (vice-versa), there will be a problem
    // with the database operations.

     public static class ChoicesTable {

        public static final String TABLE_NAME = "choices";
        public static final String ID = "choice_id";
        public static final String TEXT = "text";
        public static final String IS_CORRECT_ANSWER = "is_correct_answer";

        public static String[] getAllColumnNames() {
            return new String[] { ID, QuestionsTable.ID, TEXT, IS_CORRECT_ANSWER };
        }

        public static HashMap<String, String> getAllColumns() {
            HashMap<String, String> columns = new HashMap<>();
            columns.put(ID, "INTEGER PRIMARY KEY AUTOINCREMENT");
            columns.put(QuestionsTable.ID, "INTEGER");
            columns.put(TEXT, "TEXT");
            columns.put(IS_CORRECT_ANSWER, "INTEGER");

            return columns;
        }
    }

    public static class QuestionsTable {

        public static final String TABLE_NAME = "questions";
        public static final String ID = "question_id";
        public static final String QUERY = "query";

        public static String[] getAllColumnNames() {
            return new String[] { ID, QuizzesTable.ID, QUERY };
        }

        public static HashMap<String, String> getAllColumns() {
            HashMap<String, String> columns = new HashMap<>();
            columns.put(ID, "INTEGER PRIMARY KEY AUTOINCREMENT");
            columns.put(QuizzesTable.ID, "INTEGER");
            columns.put(QUERY, "TEXT");

            return columns;
        }
    }

    public static class QuizzesTable {

        public static final String TABLE_NAME = "quizzes";
        public static final String ID = "quiz_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String TYPE = "type";

        public static String[] getAllColumnNames() {
            return new String[] { ID, NAME, DESCRIPTION, TYPE };
        }

        public static HashMap<String, String> getAllColumns() {
            HashMap<String, String> columns = new HashMap<>();
            columns.put(ID, "INTEGER PRIMARY KEY AUTOINCREMENT");
            columns.put(NAME, "TEXT");
            columns.put(DESCRIPTION, "TEXT");
            columns.put(TYPE, "INTEGER");

            return columns;
        }
    }

    private static QuizController sSingletonInstance;

    private QuizController() {}

    public static void init() {
        sSingletonInstance = new QuizController();

        DatabaseController databaseController = DatabaseController.getInstance();
        databaseController.createTableIfNonExistent(QuizzesTable.TABLE_NAME, QuizzesTable.getAllColumns());
        databaseController.createTableIfNonExistent(QuestionsTable.TABLE_NAME, QuestionsTable.getAllColumns());
        databaseController.createTableIfNonExistent(ChoicesTable.TABLE_NAME, ChoicesTable.getAllColumns());
    }

    public static synchronized QuizController getInstance() {
        return sSingletonInstance;
    }

    // FIXME change to non-static
    public static List<Quiz> quizzes = new ArrayList<>();
}
