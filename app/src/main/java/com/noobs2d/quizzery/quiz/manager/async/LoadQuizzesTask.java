package com.noobs2d.quizzery.quiz.manager.async;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.noobs2d.quizzery.model.Choice;
import com.noobs2d.quizzery.model.Question;
import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.manager.QuizController;
import com.noobs2d.quizzery.util.DatabaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Julious Igmen
 */
public class LoadQuizzesTask extends AsyncTask<Void, Void, List<Quiz>> {

    public static final String TAG = LoadQuizzesTask.class.getSimpleName();

    private final SQLiteDatabase mDatabase;
    private final OnLoadQuizzesTaskListener mListenerOnLoadQuizzesTask;

    public LoadQuizzesTask(OnLoadQuizzesTaskListener listenerOnLoadQuizzesTask) {
        mListenerOnLoadQuizzesTask = listenerOnLoadQuizzesTask;
        DatabaseController databaseController = DatabaseController.getInstance();
        mDatabase = databaseController.getDatabase();
    }

    @Override
    protected List<Quiz> doInBackground(Void... params) {
        Log.d(TAG, "Loading quizzes...");
        List<Quiz> quizzes = new ArrayList<>();

        loadQuizzesToList(quizzes);
        loadQuestions(quizzes);

        return quizzes;
    }

    private void loadQuizzesToList(List<Quiz> quizzes) {
        String query = "SELECT * FROM " + QuizController.QuizzesTable.TABLE_NAME;

        Cursor cursor = mDatabase.rawQuery(query, null);
        cursor.moveToNext();

        for (int i = 0; i < cursor.getCount(); i++) {
            Quiz quiz = new Quiz("", "");
            quiz.id = cursor.getLong(cursor.getColumnIndex(QuizController.QuizzesTable.ID));
            quiz.name = cursor.getString(cursor.getColumnIndex(QuizController.QuizzesTable.NAME));
            quiz.description = cursor.getString(cursor.getColumnIndex(QuizController.QuizzesTable.DESCRIPTION));
            quiz.type = cursor.getInt(cursor.getColumnIndex(QuizController.QuizzesTable.TYPE));

            quizzes.add(quiz);

            cursor.moveToNext();
        }

        cursor.close();
    }

    private void loadQuestions(List<Quiz> quizzes) {
        for (Quiz quiz : quizzes) {
            // load the questions first
            quiz.questions = loadQuestionsByQuizId(quiz.id);

            // then load the questions' choices
            for (Question question : quiz.questions) {
                question.choices = loadChoicesByQuestionId(question.id);
            }
        }
    }

    private List<Question> loadQuestionsByQuizId(long quizId) {
        List<Question> questions = new ArrayList<>();

        String query = "SELECT * FROM " + QuizController.QuestionsTable.TABLE_NAME + " WHERE " + QuizController.QuizzesTable.ID + " = " + quizId;

        Cursor cursor = mDatabase.rawQuery(query, null);
        cursor.moveToNext();

        for (int i = 0; i < cursor.getCount(); i++) {
            Question question = new Question("");
            question.id = cursor.getLong(cursor.getColumnIndex(QuizController.QuestionsTable.ID));
            question.query = cursor.getString(cursor.getColumnIndex(QuizController.QuestionsTable.QUERY));

            questions.add(question);

            cursor.moveToNext();
        }

        cursor.close();

        return questions;
    }

    private List<Choice> loadChoicesByQuestionId(long questionId) {
        List<Choice> choices = new ArrayList<>();

        String query = "SELECT * FROM " + QuizController.ChoicesTable.TABLE_NAME + " WHERE " + QuizController.QuestionsTable.ID + " = " + questionId;

        Cursor cursor = mDatabase.rawQuery(query, null);
        cursor.moveToNext();

        for (int i = 0; i < cursor.getCount(); i++) {
            Choice choice = new Choice("", false);
            choice.id = cursor.getLong(cursor.getColumnIndex(QuizController.ChoicesTable.ID));
            choice.text = cursor.getString(cursor.getColumnIndex(QuizController.ChoicesTable.TEXT));
            choice.isCorrectAnswer = cursor.getLong(cursor.getColumnIndex(QuizController.ChoicesTable.IS_CORRECT_ANSWER)) == 0;

            choices.add(choice);

            cursor.moveToNext();
        }

        cursor.close();

        return choices;
    }

    @Override
    protected void onPostExecute(List<Quiz> quizzes) {
        super.onPostExecute(quizzes);

        boolean hasListener = mListenerOnLoadQuizzesTask != null;
        boolean hasQuizzesLoaded = quizzes.size() > 0;

        if (hasQuizzesLoaded && hasListener) {
            Log.d(TAG, "onLoadQuizzesSuccess");
            mListenerOnLoadQuizzesTask.onLoadQuizzesSuccess(quizzes);
        } else if (hasListener) {
            Log.d(TAG, "onLoadQuizzesFail");
            mListenerOnLoadQuizzesTask.onLoadQuizzesFail();
        }
    }
}
