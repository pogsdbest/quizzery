package com.noobs2d.quizzery.quiz.question;

import android.text.TextWatcher;
import android.view.View;

import com.noobs2d.quizzery.quiz.question.choice.OnCreateChoiceListener;

/**
 * @author Julious Igmen
 */
public interface QuestionPresenter extends OnChoiceClickListener, OnCreateChoiceListener, TextWatcher, View.OnClickListener {

}
