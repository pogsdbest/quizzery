package com.noobs2d.quizzery.quiz.question;

/**
 * @author Julious Igmen
 */
public interface OnChoiceClickListener {

    public void onChoiceClick(int position);
}
