package com.noobs2d.quizzery.quiz.question;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Choice;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class ChoicesAdapter extends RecyclerView.Adapter<ChoicesAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckBox checkBoxCorrectAnswer;
        public TextView textViewChoice;

        private OnChoiceClickListener mListenerOnChoiceClick;

        public ViewHolder(View rootView, OnChoiceClickListener listenerOnChoiceClick) {
            super(rootView);

            mListenerOnChoiceClick = listenerOnChoiceClick;

            textViewChoice = (TextView) rootView.findViewById(R.id.choice_adapter_textview_quiz_name);
            checkBoxCorrectAnswer = (CheckBox) rootView.findViewById(R.id.choice_adapter_checkbox_correct_answer);

            View container = rootView.findViewById(R.id.choice_adapter_container);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListenerOnChoiceClick.onChoiceClick(getPosition());
        }
    }

    private List<Choice> mChoices;
    private final boolean mShowCorrectAnswer;
    private final OnChoiceClickListener mListenerOnChoiceClick;

    public ChoicesAdapter(List<Choice> choices, OnChoiceClickListener listenerOnChoiceClick, boolean showCorrectAnswer) {
        mChoices = choices;
        mShowCorrectAnswer = showCorrectAnswer;
        mListenerOnChoiceClick = listenerOnChoiceClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View rootView = layoutInflater.inflate(R.layout.adapter_choice, parent, false);

        return new ViewHolder(rootView, mListenerOnChoiceClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Choice choice = mChoices.get(i);

        viewHolder.textViewChoice.setText(choice.text);

        if (choice.isCorrectAnswer && mShowCorrectAnswer) {
            viewHolder.checkBoxCorrectAnswer.setChecked(true);
        } else {
            viewHolder.checkBoxCorrectAnswer.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return mChoices.size();
    }
}
