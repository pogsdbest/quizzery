package com.noobs2d.quizzery.quiz.create;

import com.noobs2d.quizzery.model.Quiz;

import java.io.Serializable;

/**
 * @author Julious Igmen
 */
public interface OnCreateQuizDialogListener extends Serializable {

    public void onQuizCreate(Quiz quiz);
}
