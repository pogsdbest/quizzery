package com.noobs2d.quizzery.quiz.create;

import android.content.DialogInterface;
import android.text.TextWatcher;

/**
 * @author Julious Igmen
 */
public interface CreateQuizDialogPresenter extends DialogInterface.OnClickListener, TextWatcher {
}
