package com.noobs2d.quizzery.quiz.manager.async;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;

import com.noobs2d.quizzery.model.Choice;
import com.noobs2d.quizzery.model.Question;
import com.noobs2d.quizzery.model.Quiz;
import com.noobs2d.quizzery.quiz.manager.QuizController;
import com.noobs2d.quizzery.util.DatabaseController;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class SaveQuizzesTask extends AsyncTask<Quiz, Void, Boolean> {

    public static final String TAG = SaveQuizzesTask.class.getSimpleName();

    private final DatabaseController mDatabaseController;
    private final SQLiteDatabase mDatabase;
    private final OnSaveQuizzesTaskListener mListenerOnSaveQuizzesTask;

    public SaveQuizzesTask(OnSaveQuizzesTaskListener listenerOnSaveQuizTask) {
        mListenerOnSaveQuizzesTask = listenerOnSaveQuizTask;
        mDatabaseController = DatabaseController.getInstance();
        mDatabase = mDatabaseController.getDatabase();
    }

    @Override
    protected Boolean doInBackground(Quiz... quizzes) {
        boolean success = true;

        for (Quiz quiz : quizzes) {
            try {
                saveQuiz(quiz);
                saveQuestions(quiz.id, quiz.questions);
            } catch (SQLiteException e) {
                Log.e(TAG, "" + e.getMessage());
                success = false;
                break;
            }
        }

        return success;
    }

    private void saveQuiz(Quiz quiz) throws SQLiteException {
        boolean quizAlreadyInDatabase = quiz.id != -1;
        if (quizAlreadyInDatabase) {
            saveQuizNormally(quiz);
        } else {
            saveQuizAndGenerateId(quiz);
        }
    }

    private void saveQuizNormally(Quiz quiz) throws SQLiteException {
        mDatabaseController.startTransaction();

        String tableName = QuizController.QuizzesTable.TABLE_NAME;
        String[] columnNames = QuizController.QuizzesTable.getAllColumnNames();
        String query = mDatabaseController.createPreparedStatementQuery(tableName, columnNames, false);

        SQLiteStatement statement = mDatabase.compileStatement(query);
        statement.bindLong(1, quiz.id);
        statement.bindString(2, quiz.name);
        statement.bindString(3, quiz.description);
        statement.bindLong(4, quiz.type);

        statement.execute();

        mDatabaseController.endTransaction();
    }

    private void saveQuizAndGenerateId(Quiz quiz) throws SQLiteException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuizController.QuizzesTable.NAME, quiz.name);
        contentValues.put(QuizController.QuizzesTable.DESCRIPTION, quiz.description);
        contentValues.put(QuizController.QuizzesTable.TYPE, quiz.type);

        quiz.id = mDatabase.insert(QuizController.QuizzesTable.TABLE_NAME, null, contentValues);

        Log.d(TAG, "Saved a new quiz with an id of " + quiz.id);
    }

    private void saveQuestions(long quizId, List<Question> questions) throws SQLiteException {
        for (Question question : questions) {
            boolean questionAlreadyInDatabase = question.id != -1;
            if (questionAlreadyInDatabase) {
                saveQuestionNormally(quizId, question);
            } else {
                saveQuestionAndGenerateId(quizId, question);
            }

            saveChoices(question.id, question.choices);
        }
    }

    private void saveQuestionNormally(long quizId, Question question) throws SQLiteException {
        mDatabaseController.startTransaction();

        String tableName = QuizController.QuestionsTable.TABLE_NAME;
        String[] columnNames = QuizController.QuestionsTable.getAllColumnNames();
        String query = mDatabaseController.createPreparedStatementQuery(tableName, columnNames, false);

        SQLiteStatement statement = mDatabase.compileStatement(query);
        statement.bindLong(1, question.id);
        statement.bindLong(2, quizId);
        statement.bindString(3, question.query);

        statement.execute();

        mDatabaseController.endTransaction();
    }

    private void saveQuestionAndGenerateId(long quizId, Question question) throws SQLiteException {
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuizController.QuizzesTable.ID, quizId);
        contentValues.put(QuizController.QuestionsTable.QUERY, question.query);

        question.id = mDatabase.insert(QuizController.QuestionsTable.TABLE_NAME, null, contentValues);

        Log.d(TAG, "Saved a questiion with an id of " + question.id);
    }

    private void saveChoices(long questionId, List<Choice> choices) throws SQLiteException {
        for (Choice choice : choices) {
            boolean choiceAlreadyInDatabase = choice.id != -1;
            if (choiceAlreadyInDatabase) {
                saveChoiceNormally(questionId, choice);
            } else {
                saveChoiceAndGenerateId(questionId, choice);
            }
        }
    }

    private void saveChoiceNormally(long questionId, Choice choice) {
        mDatabaseController.startTransaction();

        String tableName = QuizController.ChoicesTable.TABLE_NAME;
        String[] columnNames = QuizController.ChoicesTable.getAllColumnNames();
        String query = mDatabaseController.createPreparedStatementQuery(tableName, columnNames, false);

        SQLiteStatement statement = mDatabase.compileStatement(query);
        statement.bindLong(1, choice.id);
        statement.bindLong(2, questionId);
        statement.bindString(3, choice.text);
        statement.bindLong(4, choice.isCorrectAnswer ? 0 : 1);

        statement.execute();

        mDatabaseController.endTransaction();
    }

    private void saveChoiceAndGenerateId(long questionId, Choice choice) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuizController.QuestionsTable.ID, questionId);
        contentValues.put(QuizController.ChoicesTable.TEXT, choice.text);
        contentValues.put(QuizController.ChoicesTable.IS_CORRECT_ANSWER, choice.isCorrectAnswer ? 0 : 1);

        choice.id = mDatabase.insert(QuizController.ChoicesTable.TABLE_NAME, null, contentValues);

        Log.d(TAG, "Saved a choice with an id of " + choice.id);
    }

    @Override
    protected void onPostExecute(Boolean success) {
        super.onPostExecute(success);

        boolean hasListener = mListenerOnSaveQuizzesTask != null;
        if (success && hasListener) {
            mListenerOnSaveQuizzesTask.onSaveQuizzesSuccess();
        } else if (hasListener) {
            mListenerOnSaveQuizzesTask.onSaveQuizzesFail();
        }
    }
}
