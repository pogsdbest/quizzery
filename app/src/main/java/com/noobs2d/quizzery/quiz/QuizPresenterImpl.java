package com.noobs2d.quizzery.quiz;

import android.view.View;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Question;
import com.noobs2d.quizzery.model.Quiz;

/**
 * @author Julious Igmen
 */
public class QuizPresenterImpl implements QuizPresenter {

    private QuizView mViewQuiz;
    private QuizWorker mWorkerQuiz;

    public QuizPresenterImpl(QuizView viewQuiz) {
        mViewQuiz = viewQuiz;
        mWorkerQuiz = new QuizWorkerImpl();
    }

    @Override
    public void onQuestionAdapterItemClick(Question question) {
        mViewQuiz.navigateToQuestionEditScreen(question);
    }

    @Override
    public void saveQuiz(Quiz quiz) {
        mWorkerQuiz.saveQuiz(quiz);
    }
}
