package com.noobs2d.quizzery.quiz.question.choice;

import com.noobs2d.quizzery.model.Choice;

import java.io.Serializable;

/**
 * @author Julious Igmen
 */
public interface OnCreateChoiceListener extends Serializable {

    public void onChoiceCreate(Choice choice);
}
