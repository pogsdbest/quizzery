package com.noobs2d.quizzery.quiz;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noobs2d.quizzery.R;
import com.noobs2d.quizzery.model.Question;

import java.util.List;

/**
 * @author Julious Igmen
 */
public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {

    public static interface OnQuestionAdapterItemClickListener {

        public void onQuestionAdapterItemClick(Question question);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final TextView textViewQuestion;
        public final TextView textViewQuestionCount;

        private final List<Question> mListQuestions;
        private final OnQuestionAdapterItemClickListener mListenerOnQuestionAdapterItemClick;

        public ViewHolder(View rootView, List<Question> questions, OnQuestionAdapterItemClickListener listenerOnQuestionAdapterItemClick) {
            super(rootView);

            mListQuestions = questions;
            mListenerOnQuestionAdapterItemClick = listenerOnQuestionAdapterItemClick;

            textViewQuestion = (TextView) rootView.findViewById(R.id.question_adapter_textview_question);
            textViewQuestionCount = (TextView) rootView.findViewById(R.id.question_adapter_textview_choices_count);

            View viewContainer = rootView.findViewById(R.id.question_adapter_container);
            viewContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListenerOnQuestionAdapterItemClick.onQuestionAdapterItemClick(mListQuestions.get(getPosition()));
        }
    }

    private final List<Question> mListQuestions;
    private final OnQuestionAdapterItemClickListener mListenerOnQuestionAdapterItemClick;

    public QuestionsAdapter(List<Question> listQuestions, OnQuestionAdapterItemClickListener listenerOnQuestionAdapterItemClick) {
        mListQuestions = listQuestions;
        mListenerOnQuestionAdapterItemClick = listenerOnQuestionAdapterItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View rootView = layoutInflater.inflate(R.layout.adapter_questions, parent, false);

        return new ViewHolder(rootView, mListQuestions, mListenerOnQuestionAdapterItemClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Question question = mListQuestions.get(position);

        viewHolder.textViewQuestion.setText(question.query);
        viewHolder.textViewQuestionCount.setText("" + question.choices.size());
    }

    @Override
    public int getItemCount() {
        return mListQuestions.size();
    }
}
