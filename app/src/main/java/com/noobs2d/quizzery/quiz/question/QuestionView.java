package com.noobs2d.quizzery.quiz.question;

import com.noobs2d.quizzery.model.Choice;

/**
 * @author Julious Igmen
 */
public interface QuestionView {

    public void uncheckAllChoices();

    /**
     * Makes all choices become unchecked, except the one in the parameter index, which becomes checked.
     * @param exception The index of the choice which will become and left checked
     */
    public void uncheckOtherChoices(int exception);

    public void showCreateChoiceDialog();

    public void addChoice(Choice choice);

    public void updateChoicesList();

    public void scrollDownToNewestChoice();
}
