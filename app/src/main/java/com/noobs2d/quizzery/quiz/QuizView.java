package com.noobs2d.quizzery.quiz;

import com.noobs2d.quizzery.model.Question;

/**
 * @author Julious Igmen
 */
public interface QuizView {

    public void navigateToQuestionEditScreen(Question question);
}
