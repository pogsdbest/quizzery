package com.noobs2d.quizzery.model;

import java.io.Serializable;

/**
 * @author Julious Igmen
 */
public class Choice implements Serializable {

    public long id;
    public String text;
    /** In the database, this is true if the value is 0. */
    public boolean isCorrectAnswer;

    public Choice(String text, boolean isCorrectAnswer) {
        id = -1;
        this.text = text;
        this.isCorrectAnswer = isCorrectAnswer;
    }
}
