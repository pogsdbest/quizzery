package com.noobs2d.quizzery.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Utility class that uses a singleton for all operations that is about the database.
 * Note that {@link #init(android.content.Context, String, int)} must must invoked at the start of the app.
 *
 * @author Julious Igmen
 */
public class DatabaseController extends SQLiteOpenHelper {

    private static DatabaseController sSingletonInstance;

    private SQLiteDatabase mDatabase;

    public static synchronized void destroy() {
        sSingletonInstance.mDatabase.close();
        sSingletonInstance = null;
    }

    public static synchronized DatabaseController getInstance() {
        while (sSingletonInstance == null)
            ;

        return sSingletonInstance;
    }

    /**
     * This must be invoked on app start once.
     */
    public static void init(Context context, String databaseName, int databaseVersion) {
        if (sSingletonInstance == null)
            sSingletonInstance = new DatabaseController(context, databaseName, databaseVersion);
    }

    /**
     * Singleton - no other classes can make an instance of this class except itself.
     */
    private DatabaseController(Context context, String name, int version) {
        super(context, name, null, version);

        mDatabase = getWritableDatabase();
    }

    /**
     * Creates an <code>INSERT OR REPLACE INTO</code> query string.
     * Considers that the first column is the ID.
     */
    public String createPreparedStatementQuery(String tableName, String[] columnNames, boolean idAutoIncrement) {
        String query = "INSERT OR REPLACE INTO " + tableName + " ( ";
        String valuesClause = " VALUES ( ";

        int startingIndex = idAutoIncrement ? 1 : 0;
        for (int i = startingIndex; i < columnNames.length; i++) {
            String column = columnNames[i];
            boolean notLastItem = i != columnNames.length - 1;

            query += column;
            valuesClause += "? ";

            if (notLastItem) {
                query += ", ";
                valuesClause += ", ";
            }
        }
        valuesClause += ")";
        query += ") " + valuesClause;

        return query;
    }

    /**
     * Creates a table if it doesn't exists.
     *
     * @param columns The keys are the column name while the values are the column specifications
     *                (i.e., PRIMARY KEY, NOT NULL, AUTOINCREMENT etc. etc.)
     */
    public void createTableIfNonExistent(String tableName, HashMap<String, String> columns) {
        // we'll dismiss if the table already exists
        if (tableExists(tableName, columns))
            return;

        String query = "CREATE TABLE " + tableName + " (";

        // add the columns
        for (Entry<String, String> entry : columns.entrySet())
            query += entry.getKey() + " " + entry.getValue() + ", ";

        // remove the extra comma in the end
        query = query.substring(0, query.length() - 2);

        query += ")";

        mDatabase.execSQL(query);
    }

    public void endTransaction() {
        if (mDatabase != null && mDatabase.isOpen()) {
            mDatabase.setTransactionSuccessful();
            mDatabase.endTransaction();
        }
    }

    /**
     * Waits until no other thread locks the database via {@link android.database.sqlite.SQLiteDatabase#isDbLockedByCurrentThread()}
     * so it is standard that whatever uses this method is on a thread or an {@link android.os.AsyncTask}.
     */
    public synchronized SQLiteDatabase getDatabase() {
        return mDatabase;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }

    public void startTransaction() {
        if (mDatabase != null && mDatabase.isOpen())
            mDatabase.beginTransaction();
    }

    public boolean tableExists(String tableName, HashMap<String, String> columns) {
        // convert the hashmap contents into an array
        String[] columnNames = new String[columns.size()];
        int index = 0;
        for (Entry<String, String> entry : columns.entrySet()) {
            columnNames[index] = entry.getKey(); // only the key represents the column names
            index++;
        }

        return tableExists(tableName, columnNames);
    }

    public boolean tableExists(String tableName, String[] columns) {
        boolean exists;

        // check if table exists
        String query = "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = '" + tableName + "'";
        Cursor cursor = mDatabase.rawQuery(query, null);

        if (cursor != null) {
            exists = cursor.getCount() > 0;
            cursor.close();
        } else
            exists = false;

        // check if the table contains all our expected columns
        if (exists) {
            query = "SELECT * FROM " + tableName + " LIMIT 1";
            cursor = mDatabase.rawQuery(query, null);

            // we can't use Arrays.equals(Object, Object) because the order of the arrays are isn't sorted
            // so we check it one by one if our expected columns already exist
            String[] existingColumns = cursor.getColumnNames();
            for (String existingColumn : existingColumns) {
                boolean columnExists = false;
                for (String expectedColumn : columns)
                    if (existingColumn.equals(expectedColumn)) {
                        columnExists = true;
                        break;
                    }

                if (!columnExists)
                    return false;
            }
        }

        return exists;
    }

    public boolean tableHasEntries(String table) {
        boolean hasEntries = true;

        String query = "SELECT * FROM " + table + " LIMIT 1";
        Cursor cursor = mDatabase.rawQuery(query, null);

        if (cursor != null) {
            hasEntries = cursor.getCount() > 0;
            cursor.close();
        }

        return hasEntries;
    }

}
